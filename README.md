# vcsim-rpi

Build a docker image for vcsim on ARM (Raspi & Co.) and run it on K3s (or docker as you like).

## Installation

Assuming you have a loadbalancer (e.g. metallb) running in your cluster you can start and run:

```bash
kubectl create namespace vcsim
kubectl apply -f manifests/
```

Get the external LoadBalancer IP from:

```bash
kubectl get svc -n vcsim
```

## Example usage

And external IP from above as hostname in the ansible playbook vmware.yml and execute:

```bash
ansible-playbook vmware.yml -v
```